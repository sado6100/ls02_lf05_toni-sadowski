
package rechteck;

public class Rechteck {

    private double seiteA;
    private double seiteB;

    public Rechteck(double seiteA, double seiteB)
    {
      setSeiteA(seiteA);
      setSeiteB(seiteB);
    }

    public void setSeiteA(double seite) {
        if(seite > 0)
            this.seiteA = seite;
        else
            this.seiteA = 0;
    }

    public double getSeiteA() {
        return this.seiteA;
    }

    public void setSeiteB(double seite) {
        if(seite > 0)
            this.seiteB = seite;
        else
            this.seiteB = 0;
    }

    public double getSeiteB() {
        return this.seiteA;
    }


    public double getDiagonale() {
        return Math.sqrt((this.seiteA * this.seiteA) + (this.seiteB * this.seiteB));
    }

    public double getFlaeche() {
        return (this.seiteA * this.seiteB);
    }

    public double getUmfang() {
        double A = 2 * this.seiteA;
        double B = 2 * this.seiteB;
        return (A + B);
        //return ((2 * this.seiteA) + (2 * this.seiteB));
    }

}