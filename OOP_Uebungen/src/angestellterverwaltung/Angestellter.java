package angestellterverwaltung;

public class Angestellter {
    String Name;
    Integer Gehalt;

// Construkter f�r jede m�gliche art, bis zu 2 Werten..
public Angestellter(){
    this.Name = "Mustermann";
    this.Gehalt = 1000;
}
public Angestellter(String name) {
    this.Name = name;
}
public Angestellter(Integer gehalt) {
    this.Gehalt = gehalt;
}
public Angestellter(String name, Integer gehalt) {
    this.Name = name;
    this.Gehalt = gehalt;
}


// Getter und Setter
public void setName(String name){
    this.Name = name;
}

public String getName(){
    return this.Name;
}
public void setGehalt(Integer gehalt){
    this.Gehalt = gehalt;
}

public Integer getGehalt(){
    return this.Gehalt;
}

}